<?php
/*     1
 *    / \
 *   2   3_
 *  / \  | \ 
 * 4   5 6  7 
 *
 */
 
$ar[1]['parent']=0;
$ar[1]['value']=1;
$ar[1]['nama_dosen']='Mario Simaremare';
$ar[1]['mata_kuliah']='PABWE';

 
$ar[2]['parent']=1;
$ar[2]['value']=2;
$ar[2]['nama_dosen']='Eka Sinambela';
$ar[2]['mata_kuliah']='PJK';
 
$ar[3]['parent']=1;
$ar[3]['value']=3;
$ar[3]['nama_dosen']='Samuel Situmeang';
$ar[3]['mata_kuliah']='CERTAN';
 
$ar[4]['parent']=2;
$ar[4]['value']=4;
$ar[4]['nama_dosen']='Tennov Simanjuntak';
$ar[4]['mata_kuliah']='Basdatlan';
 
$ar[5]['parent']=2;
$ar[5]['value']=5;
$ar[5]['nama_dosen']='Juliana';
$ar[5]['mata_kuliah']='PROBSTAT';
 
$ar[6]['parent']=3;
$ar[6]['value']=6;
$ar[6]['nama_dosen']='Mona';
$ar[6]['mata_kuliah']='English 3';
 
$ar[7]['parent']=3;
$ar[7]['value']=7;
$ar[7]['nama_dosen']='Ricardo';
$ar[7]['mata_kuliah']='Technopreneurship';
 

function dfs($arr,$parent,$base){
 global $explc;
 global $explv;
 $explc++;
 
 for($a=1; $a<=count($arr); $a++){
  
  if($arr[$a]['parent']==$parent){
   $explv[$explc]['parent'] = $arr[$a]['parent'];
   $explv[$explc]['value'] = $arr[$a]['value'];
   $explv[$explc]['nama_dosen'] = $arr[$a]['nama_dosen'];
   $explv[$explc]['mata_kuliah'] = $arr[$a]['mata_kuliah'];
 
   $explv[$explc]['base'] = $base;
   $base++;
   dfs($arr,$arr[$a]['value'],$base);
   $base--;
  }
 }
}
 
function menjorok($jumlah,$tanda){
 for($a=0;$a<$jumlah;$a++) echo $tanda;
}
 
echo "\n";
global $explv,$explc;
$explc = -1;
dfs($ar,0,0);
 for($a=0; $a<$explc; $a++){
 echo menjorok($explv[$a]['base'],' ').$explv[$a]['nama_dosen']." (".$explv[$a]['mata_kuliah'].")\n";
}
unset($explc);
unset($explv);
?>