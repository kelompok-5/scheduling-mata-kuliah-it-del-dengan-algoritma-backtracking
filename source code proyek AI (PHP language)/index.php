<?php
include_once("php/classes/Status.php");
include_once("vendor/autoload.php"); 
include_once("php/config/session.php");
use web\Status;

use Medoo\Medoo; 
$database = new Medoo([   
    'database_type' => 'mysql',   
    'server' => 'localhost',   
    'database_name' => 'wirosableng',   
    'username' => 'wshero',   
    'password' => 'wasweswos' 
]); 

$statuses = $database->get("status", ["id", "text", "published_at"], ["ORDER" => ["published_at" => "DESC"]]); 



$loader = new Twig_Loader_Filesystem("templates");
$twig = new Twig_Environment($loader); 
$template = $twig->load('index.html');
echo $template->render([
    "loggedIn" => $_SESSION["loggedIn"],
    "statuses" => $statuses 
]); 

?>